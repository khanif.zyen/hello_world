import 'package:flutter/material.dart';

class NotFoundPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Page Not Found'),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('404 Not Found', style: TextStyle(fontSize: 25, color: Colors.red),),
              Text("The page you are requested is invalid or not found"),
              SizedBox(width: 20.0),
              RaisedButton(child: Text('Back to Home'),onPressed: (){
                Navigator.pushNamed(context, '/');
              },)
            ],
          )
      ),
    );
  }
}