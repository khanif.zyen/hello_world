import 'package:flutter/material.dart';
import 'package:hello_world/scoped-models/products.dart';
import 'package:scoped_model/scoped_model.dart';
import './product_form.dart';
import './products_list.dart';
import '../models/product.dart';

class ProductsAdminPage extends StatelessWidget {
  Widget _buildSideDrawer(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
            title: Text('Products Admin'),
          ),
          ListTile(
            title: Text('All Products'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/products');
            },
            leading: Icon(Icons.add),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          drawer: _buildSideDrawer(context),
          appBar: AppBar(
            title: Text('Products Admin'),
            bottom: TabBar(
              tabs: <Widget>[
                Tab(
                  text: 'Create Product',
                  icon: Icon(Icons.create),
                ),
                Tab(
                  text: 'My Products',
                  icon: Icon(Icons.list),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              ProductFormPage(),
              ProductListPage(null, null, null),
            ],
          ),
        ),
    );
  }
}
