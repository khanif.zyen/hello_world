import 'package:flutter/material.dart';
import 'package:hello_world/models/product.dart';
import 'package:hello_world/scoped-models/products.dart';
import 'package:scoped_model/scoped_model.dart';
import '../ui_elements/title_default.dart';
import '../widgets/products/location.dart';
import 'dart:async';

import '../widgets/products/price_tag.dart';

class ProductPage extends StatelessWidget {
  final int productIndex;

  ProductPage(this.productIndex);

  Widget _buildRowTitleLocationPrice(double price, String title) {
    return Row(
      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        TitleDefault(title),
        SizedBox(
          width: 20.0,
        ),
        Location('Demaan, Jepara'),
        Spacer(),
        PriceTag(price.toString()),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        print('Back Button Pressed');
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: ScopedModelDescendant<ProductsModel>(
        builder: (BuildContext context, Widget child, ProductsModel model) {
          final Product product = model.products[productIndex]; 
          return Scaffold(
            appBar: AppBar(
              title: Text(product.title),
            ),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.asset(product.imageUrl),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: _buildRowTitleLocationPrice(product.price,product.title),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    children: <Widget>[
                      Text('Description:'),
                      Spacer(),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(product.description),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
