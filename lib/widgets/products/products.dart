import 'package:flutter/material.dart';
import 'package:hello_world/scoped-models/products.dart';
import 'package:hello_world/widgets/products/product_card.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../models/product.dart';

class Products extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('[Product Widget] build()');
    return ScopedModelDescendant<ProductsModel>(
      builder: (BuildContext context, Widget child, ProductsModel model) {
        return model.products.length > 0
            ? ListView.builder(
                itemBuilder: (BuildContext context, int index) =>
                    ProductCard(model.products[index], index),
                itemCount: model.products.length,
              )
            : Center(
                child: Text('No item found. Please Add Product first'),
              );
      },
    );
  }
}
