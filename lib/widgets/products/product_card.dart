import 'package:flutter/material.dart';
import 'package:hello_world/ui_elements/title_default.dart';
import './price_tag.dart';
import './location.dart';
import '../../ui_elements/title_default.dart';
import '../../models/product.dart';


class ProductCard extends StatelessWidget {
  final Product product;
  final int productIndex;

  ProductCard(this.product, this.productIndex); 

  Widget _buildRowTitlePrice(){
    return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TitleDefault(product.title),
              SizedBox(width: 10.0),
              PriceTag(product.price.toString()),
            ],
          );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          Image.asset(product.imageUrl),
          SizedBox(height: 20.0),
          _buildRowTitlePrice(),
          SizedBox(
            height: 10.0,
          ),
          Location('Demaan, Jepara'),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(
                  icon: Icon(Icons.info),
                  onPressed: () => Navigator.pushNamed<bool>(
                      context, '/product/' + productIndex.toString())),
              IconButton(
                icon: Icon(Icons.favorite_border),
                onPressed: () {},
              ),
            ],
          ),
        ],
      ),
    );
  }
}