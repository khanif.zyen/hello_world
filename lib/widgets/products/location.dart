import 'package:flutter/material.dart';

class Location extends StatelessWidget {
  final String address;

  Location(this.address);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      child: Text(address),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey, width: 1.0),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
    );
  }
}
